const chai = require("chai");
const chaiHttp = require("chai-http");
const jwt = require("jsonwebtoken");
const server = require("../../server");
const database = require("../../src/config/config");
const pool = require("../../src/config/db_handler");
const config = require("../../src/config/config");
const logger = config.logger;

const assert = require("assert");

chai.should();
chai.use(chaiHttp);

let AuthToken = jwt.sign({ id: 1 }, process.env.ENCRYPTIONKEY, { expiresIn: "7d" });
let AuthTokenWrong = jwt.sign({ id: 2 }, process.env.ENCRYPTIONKEY, { expiresIn: "7d" });

//#4 Test Participant
describe("test participants", () => {
    //#4.1 POST Participant
    describe("POST participant", () => {
        //#4.1.1
        it("4.1.1 SUCCESS POST participant", (done) => {
            chai.request(server)
                .post("/api/studenthome/1/meal/1/signup")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#4.1.2
        it("4.1.2 FAIL POST participant | Not logged in", (done) => {
            chai.request(server)
                .post("/api/studenthome/1/meal/1/signup")

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });

        //#4.1.3
        it("4.1.3 FAIL POST participant | Incorrect meal", (done) => {
            chai.request(server)
                .post("/api/studenthome/1/meal/0/signup")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    //#4.2 PUT Participant
    describe("PUT participant", () => {
        //#4.2.1
        it("4.2.1 SUCCESS PUT correct participant", (done) => {
            chai.request(server)
                .put("/api/studenthome/1/meal/1/signoff")
                .set("Authorization", "Bearer " + AuthTokenWrong)

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#4.2.2
        it("4.2.2 FAIL PUT participant | Not logged in", (done) => {
            chai.request(server)
                .put("/api/studenthome/1/meal/1/signoff")

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });

        //#4.2.3
        it("4.2.3 FAIL PUT participant | Incorrect meal", (done) => {
            chai.request(server)
                .put("/api/studenthome/1/meal/0/signoff")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });

        //#4.2.4
        it("4.2.4 FAIL PUT participant | Incorrect participation", (done) => {
            chai.request(server)
                .put("/api/studenthome/1/meal/1/signoff")
                .set("Authorization", "Bearer " + AuthTokenWrong)

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    //#4.3 GET Participant
    describe("GET participant", () => {
        //#4.3.1
        it("4.3.1 SUCCESS GET participant", (done) => {
            chai.request(server)
                .get("/api/meal/1/participants")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#4.3.2
        it("4.3.2 FAIL GET participant | Not logged in", (done) => {
            chai.request(server)
                .get("/api/meal/1/participants")

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });

        //#4.3.3
        it("4.3.3 FAIL GET participant | Incorrect meal", (done) => {
            chai.request(server)
                .get("/api/meal/0/participants")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    //#4.4 GET Participant with ID
    describe("GET participant", () => {
        //#4.4.1
        it("4.4.1 SUCCESS GET participant with ID", (done) => {
            chai.request(server)
                .get("/api/meal/1/participants/1")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#4.4.2
        it("4.4.2 FAIL GET participant with ID| Not logged in", (done) => {
            chai.request(server)
                .get("/api/meal/1/participants/1")

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });

        //#4.4.3
        it("4.4.3 FAIL GET participant with ID| Incorrect participant", (done) => {
            chai.request(server)
                .get("/api/meal/1/participants/0")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });
    });
});
