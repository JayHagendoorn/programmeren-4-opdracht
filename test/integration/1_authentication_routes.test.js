const chai = require("chai");
const chaiHttp = require("chai-http");
const jwt = require("jsonwebtoken");
const server = require("../../server");
const database = require("../../src/config/config");
const pool = require("../../src/config/db_handler");
const config = require("../../src/config/config");
const logger = config.logger;

const assert = require("assert");

chai.should();
chai.use(chaiHttp);

let AuthToken = jwt.sign({id: 1}, process.env.ENCRYPTIONKEY, { expiresIn: "7d" });
let AuthTokenWrong = jwt.sign({id: 2}, process.env.ENCRYPTIONKEY, { expiresIn: "7d" });

//#1 Test Authentication
describe("test authentication", () => {
    //#1.1 POST Login
    describe("test login", () => {
        //#1.1.1
        it("1.1.1 SUCCESS POST login", (done) => {
            chai.request(server)
                .post("/api/login")
                .send({
                    email: "jsmit@server.nl",
                    password: "secret"
                })

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#1.1.2
        it("1.1.2 FAIL POST login | Incorrect credentials", (done) => {
            chai.request(server)
                .post("/api/login")

                .send({
                    email: "jsmit@server.nl"
                })

                .end(function (err, res, body) {
                    res.should.have.status(422);
                    done();
                });
        });

        //#1.1.3
        it("1.1.3 FAIL POST login | No user", (done) => {
            chai.request(server)
                .post("/api/login")

                .send({
                    email: "gjoli@server.nl",
                    password: "secret"
                })

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });

        //#1.1.4
        it("1.1.4 FAIL POST login | Incorrect password", (done) => {
            chai.request(server)
                .post("/api/login")

                .send({
                    email: "jsmit@server.nl",
                    password: "Jaapie"
                })

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });
    });

    //#1.2 POST Register
    describe("test register", () => {
        //#1.2.1
        it("1.2.1 SUCCESS POST register", (done) => {
            chai.request(server)
                .post("/api/register")

                .send({
                    firstname: "Slikke",
                    lastname: "Gast",
                    email: "knaap@te.st",
                    studentnr: "12313213",
                    password: "wwoord"
                })

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#1.1.2
        it("1.2.2 FAIL POST register | Already exists", (done) => {
            chai.request(server)
                .post("/api/register")

                .send({
                    firstname: "Tester",
                    lastname: "Maat",
                    email: "jsmit@server.nl",
                    studentnr: "110011",
                    password: "experiment"
                })

                .end(function (err, res, body) {
                    res.should.have.status(409);
                    done();
                });
        });

        //#1.1.3
        it("1.2.3 FAIL POST register | Incorrect credentials", (done) => {
            chai.request(server)
                .post("/api/register")

                .send({
                    firstname: "Tester",
                    lastname: "Maat",
                    password: "experiment"
                })

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });
    });

    //#1.3 GET info
    describe("test info", () => {
        //#1.3.1
        it("1.3.1 SUCCESS GET info", (done) => {
            chai.request(server)
                .get("/info")

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });
    });
});
