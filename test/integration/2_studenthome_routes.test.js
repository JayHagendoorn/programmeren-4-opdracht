const chai = require("chai");
const chaiHttp = require("chai-http");
const jwt = require("jsonwebtoken");
const server = require("../../server");
const database = require("../../src/config/config");
const pool = require("../../src/config/db_handler");
const config = require("../../src/config/config");
const logger = config.logger;

const assert = require("assert");

chai.should();
chai.use(chaiHttp);

let AuthToken = jwt.sign({id: 1}, process.env.ENCRYPTIONKEY, { expiresIn: "7d" });
let AuthTokenWrong = jwt.sign({id: 2}, process.env.ENCRYPTIONKEY, { expiresIn: "7d" });

//#2 Test Studenthome
describe("test studenthome", () => {
    //#2.1 GET Studenthome
    describe("GET studenthome", () => {
        //#2.1.1
        it("2.1.1 SUCCESS GET all studenthome", (done) => {
            chai.request(server)
                .get("/api/studenthome")
                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#2.1.2
        it("2.1.2 SUCCESS GET studenthome with city", (done) => {
            chai.request(server)
                .get("/api/studenthome?city=Breda")
                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#2.1.3
        it("2.1.3 SUCCESS GET studenthome with name", (done) => {
            chai.request(server)
                .get("/api/studenthome?name=Lovensdijk")
                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#2.1.4
        it("2.1.4 FAIL GET studenthome with city | Incorrect city", (done) => {
            chai.request(server)
                .get("/api/studenthome?city=WrongCity")
                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });

        //#2.1.5
        it("2.1.5 FAIL GET studenthome with name | Incorrect name", (done) => {
            chai.request(server)
                .get("/api/studenthome?name=WrongName")
                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    //#2.2 POST studenthome
    describe("POST studenthome", () => {
        //#2.2.1
        it("2.2.1 SUCCESS POST correct studenthome", (done) => {
            chai.request(server)
                .post("/api/studenthome")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    name: "TestHuis",
                    address: "TestStraat",
                    housenumber: "1103",
                    postalcode: "1101TS",
                    telephone: "0611234451",
                    city: "TestStad"
                })

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#2.2.2
        it("2.2.2 FAIL POST studenthome | Missing credentials", (done) => {
            chai.request(server)
                .post("/api/studenthome")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    name: "TestHuis",
                    postalcode: "1101TS",
                    telephone: "0611234451",
                    city: "TestStad"
                })

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });

        //#2.2.3
        it("2.2.3 FAIL POST studenthome | Incorrect postalcode", (done) => {
            chai.request(server)
                .post("/api/studenthome")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    name: "TestHuis",
                    address: "TestStraat",
                    housenumber: "1105",
                    postalcode: "1101 Slavinkstraat",
                    telephone: "0611234451",
                    city: "TestStad"
                })

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });

        //#2.2.4
        it("2.2.4 FAIL POST studenthome | Incorrect phonenumber", (done) => {
            chai.request(server)
                .post("/api/studenthome")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    name: "TestHuis",
                    address: "TestStraat",
                    housenumber: "1103",
                    postalcode: "1101KF",
                    telephone: "09jdiu3bew87w9o3ij4hrtyue78ue9oi3k4jh",
                    city: "TestStad"
                })

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });

        //#2.2.5
        it("2.2.5 FAIL POST studenthome | Already exists", (done) => {
            chai.request(server)
                .post("/api/studenthome")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    name: "Princenhage",
                    address: "Princenhage",
                    housenumber: "11",
                    postalcode: "4706RX",
                    telephone: "061234567891",
                    city: "Breda"
                })

                .end(function (err, res, body) {
                    res.should.have.status(409);
                    done();
                });
        });

        //#2.2.6
        it("2.2.6 FAIL POST studenthome | Not logged in", (done) => {
            chai.request(server)
                .post("/api/studenthome")

                .send({
                    name: "TestHuis",
                    address: "TestStraat",
                    housenumber: "1103",
                    postalcode: "1101TS",
                    telephone: "0611234451",
                    city: "TestStad"
                })

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });
    });

    //#2.3 GET studenthome
    describe("GET studenthome with ID", () => {
        //#2.3.1
        it("2.3.1 SUCCESS GET correct studenthome with ID", (done) => {
            chai.request(server)
                .get("/api/studenthome/1")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#2.3.2
        it("2.3.2 FAIL GET studenthome with ID | Incorrect ID", (done) => {
            chai.request(server)
                .get("/api/studenthome/0")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    //#2.4 PUT studenthome
    describe("PUT studenthome with ID", () => {
        //#2.4.1
        it("2.4.1 SUCCESS PUT correct studenthome with ID", (done) => {
            chai.request(server)
                .put("/api/studenthome/6")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    address: "NieuweStraat"
                })

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#2.4.2
        it("2.4.2 FAIL PUT studenthome with ID | Incorrect postal code", (done) => {
            chai.request(server)
                .put("/api/studenthome/6")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    postalcode: "1101111111"
                })

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });

        //#2.4.3
        it("2.4.3 FAIL PUT studenthome with ID | Incorrect phone number", (done) => {
            chai.request(server)
                .put("/api/studenthome/6")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    telephone: "110111145586721327683546363"
                })

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });

        //#2.4.4
        it("2.4.4 FAIL PUT studenthome with ID | Not logged in", (done) => {
            chai.request(server)
                .put("/api/studenthome/6")

                .send({
                    telephone: "99999999999"
                })

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });

        //#2.4.5
        it("2.4.5 FAIL PUT studenthome with ID | Incorrect ID", (done) => {
            chai.request(server)
                .put("/api/studenthome/0")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    telephone: "99999999999"
                })

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    //#2.5 DELETE studenthome
    describe("DELETE studenthome with ID", () => {
        //#2.5.1
        it("2.5.1 SUCCESS DELETE correct studenthome with ID", (done) => {
            chai.request(server)
                .delete("/api/studenthome/6")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#2.5.2
        it("2.5.2 FAIL DELETE studenthome with ID | Not logged in", (done) => {
            chai.request(server)
                .delete("/api/studenthome/5")

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });

        //#2.5.3
        it("2.5.3 FAIL DELETE studenthome with ID | Incorrect user", (done) => {
            chai.request(server)
                .delete("/api/studenthome/5")
                .set("Authorization", "Bearer " + AuthTokenWrong)

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });

        //#2.5.4
        it("2.5.4 FAIL DELETE studenthome with ID | Incorrect ID", (done) => {
            chai.request(server)
                .delete("/api/studenthome/0")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });
    });
});
