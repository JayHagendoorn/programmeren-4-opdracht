const chai = require("chai");
const chaiHttp = require("chai-http");
const jwt = require("jsonwebtoken");
const server = require("../../server");
const database = require("../../src/config/config");
const pool = require("../../src/config/db_handler");
const config = require("../../src/config/config");
const logger = config.logger;

const assert = require("assert");

chai.should();
chai.use(chaiHttp);

let AuthToken = jwt.sign({id: 1}, process.env.ENCRYPTIONKEY, { expiresIn: "7d" });
let AuthTokenWrong = jwt.sign({id: 2}, process.env.ENCRYPTIONKEY, { expiresIn: "7d" });

//#3 Test Meal
describe("test meal", () => {
    //#3.1 GET all Meals
    describe("GET all meals", () => {
        //#3.1.1
        it("3.1.1 SUCCESS GET all meal", (done) => {
            chai.request(server)
                .get("/api/studenthome/1/meal")
                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });
    });

    //#3.2 GET Meal
    describe("GET meal", () => {
        //#3.2.1
        it("3.2.1 SUCCESS GET meal", (done) => {
            chai.request(server)
                .get("/api/studenthome/1/meal/1")
                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#3.2.2
        it("3.2.2 FAIL GET meal | Incorrect ID", (done) => {
            chai.request(server)
                .get("/api/studenthome/1/meal/0")
                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    //#3.3 POST Meal
    describe("POST meal", () => {
        //#3.3.1
        it("3.3.1 SUCCESS POST meal", (done) => {
            chai.request(server)
                .post("/api/studenthome/2/meal")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    name: "laved",
                    description: "Heavyden",
                    creationDate: "2015-10-25",
                    offerDate: "2015-10-25",
                    price: "2",
                    allergies: "Aaaaass",
                    ingredients: ["komkommer", "sla"],
                    maxParticipants: "5"
                })

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#3.3.2
        it("3.3.2 FAIL POST meal | Not logged in", (done) => {
            chai.request(server)
                .post("/api/studenthome/2/meal")

                .send({
                    name: "laved",
                    description: "Heavyden",
                    creationDate: "2015-10-25",
                    offerDate: "2015-10-25",
                    price: "2",
                    allergies: "Aaaaass",
                    ingredients: ["komkommer", "sla"],
                    maxParticipants: "5"
                })

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });

        //#3.3.3
        it("3.3.3 FAIL POST meal | Invalid credentials", (done) => {
            chai.request(server)
                .post("/api/studenthome/2/meal")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    name: "laved",
                    description: "Heavyden",
                    creationDate: "2015-10-25",
                    offerDate: "2015-10-25",
                    ingredients: ["komkommer", "sla"],
                    maxParticipants: "5"
                })

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    //#3.3 PUT Meal
    describe("PUT meal", () => {
        //#3.3.1
        it("3.3.1 SUCCESS PUT meal", (done) => {
            chai.request(server)
                .put("/api/studenthome/2/meal/3")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    allergies: "Glutenvrij"
                })

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#3.3.2
        it("3.3.2 FAIL PUT meal | Incorrect credentials", (done) => {
            chai.request(server)
                .put("/api/studenthome/2/meal/3")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    offerDate: "0-10-25"
                })

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });

        //#3.3.3
        it("3.3.3 FAIL PUT meal | Incorrect ID", (done) => {
            chai.request(server)
                .put("/api/studenthome/2/meal/0")
                .set("Authorization", "Bearer " + AuthToken)

                .send({
                    offerDate: "2017-10-25"
                })

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });

        //#3.3.4
        it("3.3.4 FAIL PUT meal | Not logged in", (done) => {
            chai.request(server)
                .put("/api/studenthome/2/meal/3")

                .send({
                    offerDate: "2018-10-25"
                })

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });
    });

    //#3.3 DELETE Meal
    describe("DELETE meal", () => {
        //#3.4.1
        it("3.4.1 SUCCESS DELETE meal", (done) => {
            chai.request(server)
                .delete("/api/studenthome/1/meal/3")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(200);
                    done();
                });
        });

        //#3.4.2
        it("3.4.2 FAIL DELETE meal | Incorrect user", (done) => {
            chai.request(server)
                .delete("/api/studenthome/1/meal/1")
                .set("Authorization", "Bearer " + AuthTokenWrong)

                .end(function (err, res, body) {
                    res.should.have.status(401);
                    done();
                });
        });

        //#3.4.3
        it("3.4.3 FAIL DELETE meal | Incorrect ID", (done) => {
            chai.request(server)
                .delete("/api/studenthome/1/meal/0")
                .set("Authorization", "Bearer " + AuthToken)

                .end(function (err, res, body) {
                    res.should.have.status(404);
                    done();
                });
        });
    });
});
