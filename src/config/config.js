const loglevel = process.env.loglevel || "debug";

module.exports = {
    // Sets logger display
    logger: require("tracer").console({
        format: ['{{timestamp}} [{{title}}] {{file}}:{{line}} : {{message}}'],
        preprocess: function (data) {
            data.title = data.title.toUpperCase()
        },
        dateformat: 'isoUtcDatetime',
        level: loglevel
    })
};
