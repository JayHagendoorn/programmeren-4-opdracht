const mysql = require("mysql");
const config = require("../config/config");
const logger = config.logger;
require('dotenv').config();

// Set connection settings with backup when values are null
const connectionSettings = {
    host: process.env.DB_HOST || 'localhost',
    user: process.env.DB_USER || 'studenthome_user',
    password: process.env.DB_PASSWORD || 'secret',
    database: process.env.DB_DATABASE || 'studenthome'
};

// Log the connection
logger.debug("Connection: Host = " + connectionSettings.host + " User = " + connectionSettings.user + " DB = " + connectionSettings.database)

// Create connection pool
let pool = mysql.createPool(connectionSettings);

module.exports = pool;
