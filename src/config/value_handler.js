const db = require("./db_handler");
const config = require("./config");
const logger = config.logger;

let controller = {
    // Returns true when rows is not null
    // Sends error messages with status code when not
    checkRowsIsNotNull(res, error, rows, errorCode, errorMsg) {
        if (error) {
            logger.error("Error: " + error);
            res.status(500).json({ error: error.toString() });
            return error;
        } else if (rows.length == 0) {
            res.status(errorCode).json({ error: errorMsg });
            return false;
        } else {
            return true;
        }
    },

    // Returns true when rows is null
    // Sends error messages with status code when not
    checkRowsIsNull(res, error, rows, errorCode, errorMsg) {
        if (error) {
            logger.error("Error: " + error);
            res.status(500).json({ error: ex.toString() });
            return error;
        } else if (rows.length > 0) {
            res.status(errorCode).json({ error: errorMsg });
            return false;
        } else {
            return true;
        }
    },

    // Returns true when user matches linked user
    // Sends error when user value doesn't match the logged in user
    checkAuthorization(res, value1, value2) {
        if(value1 == value2){
            return true
        } else {
            res.status(401).json({ error: "Unauthorized" });
            return false;
        }
    }
};

module.exports = controller;
