const db = require("../config/db_handler");
const config = require("../config/config");
const logger = config.logger;

let controller = {
    // Creates the proper GET call
    getRecord(table, specification, callback) {
        // SELECT * FROM user WHERE First_Name = 'Mark';
        let sqlQuery = "SELECT * FROM " + table + " " + specification + ";";

        logger.debug("GET call with Query: " + sqlQuery);

        // Send created query to function that executes it
        this.runQuery(sqlQuery, callback)
    },

    // Creates the proper POST call
    postRecord(table, columns, specifications, callback) {
        // INSERT INTO 'studenthome'
        // ('Name', 'Address', 'House_Nr', 'UserID', 'Postal_Code', 'Telephone', 'City')
        // VALUES ('Princenhage', 'Princenhage', 11, 1,'4706RX','061234567891','Breda');
        let sqlQuery = "INSERT INTO " + table + " (" + columns + ") VALUES (" + specifications + ");";

        logger.debug("POST call with Query: " + sqlQuery);

        // Send created query to function that executes it
        this.runQuery(sqlQuery, callback)
    },

    // Creates the proper PUT call
    putRecord(table, alterations, column, specification, callback) {
        // UPDATE 'studenthome'
        // SET Name = 'homeName', Address = 'homeAddress', House_Nr = 11, UserID =  1, Postal_Code = '4706RX', Telephone = '061234567891', City = 'homeCity'
        // WHERE ID = 1;
        let sqlQuery = "UPDATE " + table + " SET " + alterations + " WHERE " + column + " = '" + specification + "';";

        logger.debug("PUT call with Query: " + sqlQuery);

        // Send created query to function that executes it
        this.runQuery(sqlQuery, callback)
    },

    // Creates the proper DELETE call
    deleteRecord(table, column, specification, callback) {
        // SELECT * FROM studenthome WHERE ID = 1;
        let sqlQuery = "DELETE FROM " + table + " WHERE " + column + " = '" + specification + "';";

        logger.debug("DELETE call with Query: " + sqlQuery);

        // Send created query to function that executes it
        this.runQuery(sqlQuery, callback)
    },
    // Creates the proper DELETE call with multiple specification
    deleteRecordWithMoreIDs(table, specification, callback) {
        // SELECT * FROM studenthome WHERE ID = 1;
        let sqlQuery = "DELETE FROM " + table + " WHERE " + specification + ";";

        logger.debug("DELETE call with Query: " + sqlQuery);

        // Send created query to function that executes it
        this.runQuery(sqlQuery, callback)
    },

    // Executes the query
    runQuery(sqlQuery, callback) {
        // Log full query
        logger.debug("Executing query: " + sqlQuery);

        // Executes query
        db.query(sqlQuery, (error, rows, fields) => {
            if (error) {
                // Send an error when something's wrong
                logger.error("Error: " + error.toString())
                //throw error;
                callback(error, undefined);
            } else {
                logger.debug("Rows length: " + rows.length);
                callback(undefined, rows)
            }
        });
    }
};

module.exports = controller;
