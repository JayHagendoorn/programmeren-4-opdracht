const express = require("express");
const router = express.Router();
const config = require("../config/config");
const logger = config.logger;

const queryHandler = require("../config/query_handler");
const valueHandler = require("../config/value_handler");

const Meal = require("../models/meal");
const authenticator = require("../controllers/authentication_controller");

// POST method
router.post("/:homeId/meal", authenticator.validateToken, (req, res, next) => {
    logger.info("Post request at /api/studenthome/:homeId/meal");

    // Continues if the logged in user matches the user attached to the home
    if (valueHandler.checkAuthorization(res, req.userId, req.userIdHome)) {
        try {
            let ingredients = "";
            logger.info("ingredients: " + ingredients);
            req.body.ingredients.forEach((element) => (ingredients += ", " + element));

            ingredients = ingredients.substring(2);
            logger.info("ingredients: " + ingredients);

            // Tries to create a new meal
            // Sends error if credentials are incorrect

            let name = req.body.name;
            let description = req.body.description;
            let allergies = req.body.allergies;
            let creationDate = req.body.creationDate;
            let offerDate = req.body.offerDate;
            let price = req.body.price;
            let userId = req.userId;
            let homeId = req.homeId;
            let maxParticipants = req.body.maxParticipants;

            const meal = new Meal(name, description, ingredients, allergies, creationDate, offerDate, price, userId, homeId, maxParticipants);

            // Creates portions of the query with columns and values
            let columns = "Name, Description, CreatedOn, OfferedOn, Price, Allergies, Ingredients, UserID, StudenthomeID, MaxParticipants";
            let specifications =
                "'" +
                meal.name +
                "', '" +
                meal.description +
                "', '" +
                meal.creationDate +
                "', '" +
                meal.offerDate +
                "', '" +
                meal.price +
                "', '" +
                meal.allergies +
                "', '" +
                meal.ingredients +
                "', " +
                meal.userId +
                ", " +
                meal.homeId +
                ", " +
                meal.maxParticipants +
                "";

            // Posts the meal with the specifications
            queryHandler.postRecord("meal", columns, specifications, function (error, rows) {
                if (valueHandler.checkRowsIsNotNull(res, error, rows, 401, "Meal could not be created")) {
                    res.status(200).json({
                        name: meal.name,
                        description: meal.description,
                        ingredients: meal.ingredients,
                        allergies: meal.allergies,
                        creationDate: meal.creationDate,
                        offerDate: meal.offerDate,
                        price: meal.price,
                        userId: meal.userId,
                        homeId: meal.homeId,
                        maxParticipants: meal.maxParticipants
                    });
                }
            });
        } catch (e) {
            res.status(404).json({ error: "Incorrect values" });
        }
    }
});

// PUT method with ID
router.put("/:homeId/meal/:mealId", authenticator.validateToken, (req, res, next) => {
    logger.info("PUT request at /api/studenthome/:homeId/meal");

    // Creates object with the retrieved meal ID
    const mealId = req.params.mealId;

    // Creates portions of the query with where clause
    let whereClause = "WHERE ID = " + mealId;

    // Requests the meal
    queryHandler.getRecord("meal", whereClause, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "Meal does not exist")) {
            // Creates object with retreived information
            let resultMeal = rows[0];

            logger.debug("Requested meal: [" + resultMeal.ID + "] " + resultMeal.Name);

            // Continues if the logged in user matches the user attached to the meal
            if (valueHandler.checkAuthorization(res, resultMeal.UserID, req.userId)) {
                try {
                    // Tries to create a new meal
                    // Sends error if credentials are incorrect
                    // Reuses old credentials if nothing was received

                    let name = req.body.name || resultMeal.Name;
                    let description = req.body.description || resultMeal.Description;
                    let ingredients = req.body.ingredients || resultMeal.Ingredients;
                    let allergies = req.body.allergies || resultMeal.Allergies;
                    let creationDate =
                        req.body.creationDate || resultMeal.CreatedOn.getFullYear() + "-" + (resultMeal.CreatedOn.getMonth() + 1) + "-" + resultMeal.CreatedOn.getDate();
                    let offerDate =
                        req.body.offerDate || resultMeal.OfferedOn.getFullYear() + "-" + (resultMeal.OfferedOn.getMonth() + 1) + "-" + resultMeal.OfferedOn.getDate();
                    let price = req.body.price || resultMeal.Price;
                    let userId = resultMeal.UserID;
                    let homeId = resultMeal.StudenthomeID;
                    let maxParticipants = req.body.maxParticipants || resultMeal.MaxParticipants;

                    const newMeal = new Meal(name, description, ingredients, allergies, creationDate, offerDate, price, userId, homeId, maxParticipants);

                    // Creates portions of the query with set clause
                    let setClause =
                        "Name = '" +
                        newMeal.name +
                        "', Description = '" +
                        newMeal.description +
                        "', Ingredients = '" +
                        newMeal.ingredients +
                        "', Allergies = '" +
                        newMeal.allergies +
                        "', CreatedOn = '" +
                        newMeal.creationDate +
                        "', OfferedOn = '" +
                        newMeal.offerDate +
                        "', Price = '" +
                        newMeal.price +
                        "', MaxParticipants = '" +
                        newMeal.maxParticipants +
                        "'";

                    logger.debug("The SetClause: " + setClause);

                    // Puts changed meal with the new specifications
                    queryHandler.putRecord("meal", setClause, "ID", mealId, function (rows) {
                        res.status(200).json({ result: newMeal });
                    });
                } catch (e) {
                    res.status(404).json({ message: e.toString() });
                }
            }
        }
    });
});

// GET method from home
router.get("/:homeId/meal", (req, res, next) => {
    logger.info("GET request at /api/studenthome/:homeId/meal");

    // Creates portions of the query with where clause
    let whereClause = "Where StudenthomeID = " + req.homeId;

    // Requests the meal from the studenthome
    queryHandler.getRecord("meal", whereClause, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "No results")) {
            res.status(200).json({ result: rows });
        }
    });
});

// GET method with ID
router.get("/:homeId/meal/:mealId", (req, res, next) => {
    logger.info("GET request at /api/studenthome/:homeId/meal/:mealId");

    // Creates portions of the query with where clause
    let whereClause = "Where ID = " + req.params.mealId;

    // Requests the meal with ID
    queryHandler.getRecord("meal", whereClause, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "No results")) {
            res.status(200).json({ result: rows });
        }
    });
});

// DELETE method with ID
router.delete("/:homeId/meal/:mealId", authenticator.validateToken, (req, res, next) => {
    logger.info("DELETE request at /api/studenthome");

    // Creates object with the retrieved meal ID
    const mealId = req.params.mealId;

    // Creates portions of the query with where clause
    let whereClause = "WHERE ID = " + mealId + "";

    // Requests the meal with ID
    queryHandler.getRecord("meal", whereClause, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "No results")) {
            logger.info("userid: " + rows[0].UserID);
            logger.info("userid: " + req.userId);

            // Continues if the logged in user matches the user attached to the meal
            if (valueHandler.checkAuthorization(res, rows[0].UserID, req.userId)) {
                // Deletes the meal with ID
                queryHandler.deleteRecord("meal", "ID", mealId, function (error, rows) {
                    if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "Could not return records")) {
                        res.status(200).json({ result: rows });
                    }
                });
            }
        }
    });
});

module.exports = router;
