const express = require("express");
const router = express.Router();
const config = require("../config/config");
const logger = config.logger;
const assert = require("assert");
const jwt = require("jsonwebtoken");

const queryHandler = require("../config/query_handler");
const valueHandler = require("../config/value_handler");

const User = require("../models/user");

// POST login
router.post("/login", validateLogin, (req, res, next) => {
    logger.info("Post request at /api/login");

    // Creates portions of the query with where clause
    let whereClause = "WHERE Email = '" + req.body.email + "' AND Password = '" + req.body.password + "'";

    // Requests a user with the given credentials
    queryHandler.getRecord("user", whereClause, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 401, "Invalid email or password")) {
            // Creates payload for JWT token
            const payload = {
                id: rows[0].ID
            };

            // Creates info with token and username
            const info = {
                token: jwt.sign(payload, process.env.ENCRYPTIONKEY, { expiresIn: "7d" }),
                username: rows[0].First_Name + " " + rows[0].Last_Name
            };

            // Sends success message with the new user
            res.status(200).json(info);
        }
    });
});

router.post("/register", (req, res, next) => {
    logger.info("Post request at /api/register");

    // Tries to create a new user
    // Sends error if credentials are incorrect
    try {
        let firstName = req.body.firstname;
        let lastName = req.body.lastname;
        let email = req.body.email;
        let studentNumber = req.body.studentnr;
        let password = req.body.password;

        const user = new User(firstName, lastName, email, studentNumber, password);

        // Creates portions of the query with required columns, values and where clause
        let columns = "First_Name, Last_Name, Email, Student_Number, Password";
        let specifications = "'" + user.firstName + "', '" + user.lastName + "', '" + user.email + "', " + user.studentNumber + ", '" + user.password + "'";
        let whereClause = "WHERE Email = '" + user.email + "'";

        // Requests the user
        queryHandler.getRecord("user", whereClause, function (error, rows) {
            if (valueHandler.checkRowsIsNull(res, error, rows, 409, "User already exists")) {
                // Posts a new user
                queryHandler.postRecord("user", columns, specifications, function (error, rows) {
                    if (valueHandler.checkRowsIsNotNull(res, error, rows, 401, "User cannot be created")) {
                        // Requests the new user
                        queryHandler.getRecord("user", whereClause, function (error, rows) {
                            if (valueHandler.checkRowsIsNotNull(res, error, rows, 401, "User cannot be found")) {
                                // Creates payload for JWT token
                                const payload = {
                                    id: rows[0].ID
                                };

                                // Creates info with token and username
                                const info = {
                                    token: jwt.sign(payload, process.env.ENCRYPTIONKEY, { expiresIn: "7d" }),
                                    username: rows[0].First_Name + " " + rows[0].Last_Name
                                };

                                // Sends success message with the new user
                                res.status(200).json({ message: "User successfully created", info });
                            }
                        });
                    }
                });
            }
        });
    } catch (e) {
        // Sends error message when user credentials are incorrect
        res.status(401).json({ error: e.toString });
    }
});

// Check received login credentials
function validateLogin(req, res, next) {
    try {
        assert(typeof req.body.email === "string", "Email must be a string");
        assert(typeof req.body.password === "string", "Password must be a string");
        next();
    } catch (ex) {
        res.status(422).json({ error: ex.toString() });
    }
}

module.exports = router;
