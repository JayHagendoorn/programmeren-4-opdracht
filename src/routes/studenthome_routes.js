const express = require("express");
const router = express.Router();
const config = require("../config/config");
const logger = config.logger;

const queryHandler = require("../config/query_handler");
const valueHandler = require("../config/value_handler");

const StudentHome = require("../models/studenthome");
const authenticator = require("../controllers/authentication_controller");

// POST method
router.post("/studenthome", authenticator.validateToken, (req, res, next) => {
    logger.info("Post request at /api/studenthome");

    // Tries to create a new home
    // Sends error if credentials are incorrect
    try {
        let name = req.body.name;
        let address = req.body.address;
        let housenumber = req.body.housenumber;
        let userId = req.userId;
        let postalcode = req.body.postalcode;
        let telephone = req.body.telephone;
        let city = req.body.city;

        const studenthome = new StudentHome(name, address, housenumber, userId, postalcode, telephone, city);

        // Creates portions of the query with columns, values and where clause
        let columns = "Name, Address, House_Nr, UserID, Postal_Code, Telephone, City";
        let specifications =
            "'" +
            studenthome.name +
            "', '" +
            studenthome.address +
            "', " +
            studenthome.housenumber +
            ", " +
            studenthome.userId +
            ", '" +
            studenthome.postalcode +
            "', '" +
            studenthome.telephone +
            "', '" +
            studenthome.city +
            "'";
        let whereClause = "WHERE Address = '" + studenthome.address + "' AND House_Nr = '" + studenthome.housenumber + "'";

        // Requests a home with the given address and house number
        queryHandler.getRecord("studenthome", whereClause, function (error, rows) {
            // Continues when no home has been given (no duplicates)
            if (valueHandler.checkRowsIsNull(res, error, rows, 409, "Student home already exists")) {
                // Creates where clause to request a user
                let whereClauseUser = "WHERE ID = '" + studenthome.userId + "'";

                // Requests a user with the given ID
                queryHandler.getRecord("user", whereClauseUser, function (error, rows) {
                    // Continues if the user has been found
                    if (valueHandler.checkRowsIsNotNull(res, error, rows, 400, "User does not exist")) {
                        // Posts home when no home on same address has been found and the user ID is valid
                        queryHandler.postRecord("studenthome", columns, specifications, function (error, rows) {
                            if (valueHandler.checkRowsIsNotNull(res, error, rows, 401, "Could not create studenthome")) {
                                res.status(200).json({
                                    name: studenthome.name,
                                    address: studenthome.address,
                                    housenumber: studenthome.housenumber,
                                    userId: studenthome.userId,
                                    postalcode: studenthome.postalcode,
                                    telephone: studenthome.telephone,
                                    city: studenthome.city
                                });
                            }
                        });
                    }
                });
            }
        });
    } catch (e) {
        res.status(404).json({ message: e.toString() });
    }
});

// GET method with name and city in query
router.get("/studenthome", (req, res, next) => {
    logger.info("GET request at /api/studenthome");

    // Creates objects with given information
    let name = req.query.name;
    let city = req.query.city;

    // Presets where clause
    let whereClauseUser = "";

    // Changes where clause to fit the query
    if (name && city) {
        whereClauseUser = "WHERE Name = '" + name + "' AND City = '" + city + "'";
    } else if (name) {
        whereClauseUser = "WHERE Name = '" + name + "'";
    } else if (city) {
        whereClauseUser = "WHERE City = '" + city + "'";
    }

    // Requests home with given information
    queryHandler.getRecord("studenthome", whereClauseUser, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "No results")) {
            res.status(200).json({ result: rows });
        }
    });
});

// GET method with ID
router.get("/studenthome/:homeId", (req, res, next) => {
    logger.info("GET request at /api/studenthome");

    // Creates object from retreived home ID
    let homeId = req.params.homeId;

    // Creates portions of the query with where clause
    let whereClause = "WHERE ID = " + homeId;

    // Requests home with given information
    queryHandler.getRecord("studenthome", whereClause, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "No results")) {
            res.status(200).json({ result: rows });
        }
    });
});

// PUT method with ID
router.put("/studenthome/:homeId", authenticator.validateToken, (req, res, next) => {
    logger.info("PUT request at /api/studenthome");

    // Creates object from retreived home ID
    const homeId = req.params.homeId;

    // Creates portions of the query with set clause
    let whereClause = "WHERE ID = " + homeId;

    // Requests a home with the given credentials
    queryHandler.getRecord("studenthome", whereClause, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "No results")) {
            // Creates object from retreived rows
            let resultHome = rows[0];

            logger.debug("Asked student home: " + resultHome.Telephone);

            // Continues if the logged in user matches the user attached to the home
            if (valueHandler.checkAuthorization(res, resultHome.UserID, req.userId)) {
                try {
                    // Tries to create a new home
                    // Sends error if credentials are incorrect
                    // Reuses old credentials if nothing was received
                    let name = req.body.name || resultHome.Name;
                    let address = req.body.address || resultHome.Address;
                    let housenumber = req.body.housenumber || resultHome.House_Nr;
                    let userId = resultHome.UserID;
                    let postalcode = req.body.postalcode || resultHome.Postal_Code;
                    let telephone = req.body.telephone || resultHome.Telephone;
                    let city = req.body.city || resultHome.City;

                    const newHome = new StudentHome(name, address, housenumber, userId, postalcode, telephone, city);

                    // Creates portions of the query with set clause
                    let setClause =
                        "Name = '" +
                        newHome.name +
                        "', Address = '" +
                        newHome.address +
                        "', House_Nr = " +
                        newHome.housenumber +
                        ", UserID = " +
                        newHome.userId +
                        ", Postal_Code = '" +
                        newHome.postalcode +
                        "', Telephone = '" +
                        newHome.telephone +
                        "', City = '" +
                        newHome.city +
                        "'";

                    logger.debug("The SetClause: " + setClause);

                    // Puts changed meal with the new specifications
                    queryHandler.putRecord("studenthome", setClause, "ID", homeId, function (rows) {
                        res.status(200).json({ result: newHome });
                    });
                } catch (e) {
                    res.status(401).json({ message: e.toString() });
                }
            }
        }
    });
});

// DELETE method with ID
router.delete("/studenthome/:homeId", authenticator.validateToken, (req, res, next) => {
    logger.info("DELETE request at /api/studenthome");

    // Creates object with the retrieved home ID
    const homeId = req.params.homeId;

    // Creates portions of the query with where clause
    let whereClause = "WHERE ID = " + homeId + "";

    // Requests the home with ID
    queryHandler.getRecord("studenthome", whereClause, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "No results")) {
            logger.info("userid: " + rows[0].UserID);

            // Continues if the logged in user matches the user attached to the home
            if (valueHandler.checkAuthorization(res, rows[0].UserID, req.userId)) {
                // Deletes the home with ID
                queryHandler.deleteRecord("studenthome", "ID", homeId, function (error, rows) {
                    if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "Could not return records")) {
                        res.status(200).json({ result: rows });
                    }
                });
            }
        }
    });
});

module.exports = router;
