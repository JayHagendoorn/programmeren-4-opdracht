const express = require("express");
const router = express.Router();
const config = require("../config/config");
const logger = config.logger;

const queryHandler = require("../config/query_handler");
const valueHandler = require("../config/value_handler");

// Sets the home ID and user ID connected to the home before every call
router.all("/studenthome/:homeId/*", (req, res, next) => {
    // Sets the home ID at the request
    req.homeId = req.params.homeId;

    // Creates portions of the query with where clause
    let whereClauseHome = "WHERE ID = " + req.homeId;

    logger.info("Home ID query: " + whereClauseHome);

    // Requests the home with the home ID
    queryHandler.getRecord("studenthome", whereClauseHome, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "Studenthome not found")) {
            // Sets the user ID at the request
            req.userIdHome = rows[0].UserID;
            logger.info("User ID from home: " + req.userIdHome);
            logger.debug("Requested home: [" + rows[0].ID + "] " + rows[0].Name);

            // Continues the call to the following route
            next();
        }
    });
});
// Sets the home ID and user ID connected to the home before every call
router.all("*/meal/:mealId/*", (req, res, next) => {
    // Sets the home ID at the request
    req.mealId = req.params.mealId;

    // Creates portions of the query with where clause
    let whereClauseMeal = "WHERE ID = " + req.mealId;

    logger.info("Meal ID query: " + whereClauseMeal);

    // Requests the home with the home ID
    queryHandler.getRecord("meal", whereClauseMeal, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "Meal not found")) {
            logger.debug("rows ");
            next();
        }
    });
});

module.exports = router;
