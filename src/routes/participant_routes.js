const express = require("express");
const router = express.Router();
const config = require("../config/config");
const logger = config.logger;

const queryHandler = require("../config/query_handler");
const valueHandler = require("../config/value_handler");

const Participation = require("../models/participant");
const authenticator = require("../controllers/authentication_controller");

// POST method
router.post("/studenthome/:homeId/meal/:mealId/signup", authenticator.validateToken, (req, res, next) => {
    logger.info("Post request at /api/studenthome/:homeId/meal");
    logger.debug("homeId: " + req.homeId);
    logger.debug("mealid: " + req.mealId);

    // Continues if the logged in user matches the user attached to the home
    if (valueHandler.checkAuthorization(res, req.userId, req.userIdHome)) {
        try {
            // Requests the meal with ID
            getParticipantCount(res, req.mealId, function (error, value) {
                if (value) {
                    let whereClauseParticipant = "WHERE UserID = " + req.userId + " AND StudenthomeID = " + req.homeId + " AND MealID = " + req.mealId;
                    queryHandler.getRecord("participants", whereClauseParticipant, function (error, rows) {
                        if (valueHandler.checkRowsIsNull(res, error, rows, 409, "User is already participating")) {
                            let currentDate = new Date();
                            let day = String(currentDate.getDate()).padStart(2, "0");
                            let month = String(currentDate.getMonth() + 1).padStart(2, "0");
                            let year = currentDate.getFullYear();

                            currentDate = year + "-" + month + "-" + day;
                            logger.info("Date: " + currentDate);

                            let userId = req.userId;
                            let studenthomeId = req.homeId
                            let mealId = req.mealId
                            let signedUpOn = currentDate
                            // Tries to create a new meal
                            // Sends error if credentials are incorrect
                            const participation = new Participation(userId, studenthomeId, mealId, signedUpOn);

                            // Creates portions of the query with columns and values
                            let columns = "UserID, StudenthomeID, MealID, SignedUpOn";
                            let specifications =
                                "" + participation.userId + ", " + participation.studenthomeId + ", " + participation.mealId + ", '" + participation.signedUpOn + "'";

                            // Posts the meal with the specifications
                            queryHandler.postRecord("participants", columns, specifications, function (error, rows) {
                                if (valueHandler.checkRowsIsNotNull(res, error, rows, 401, "Could not participate")) {
                                    res.status(200).json({
                                        userId: participation.userId,
                                        studenthomeId: participation.studenthomeId,
                                        mealId: participation.mealId,
                                        signedUpOn: participation.signedUpOn
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } catch (e) {
            res.status(404).json({ message: "Incorrect values" });
        }
    }
});

router.put("/studenthome/:homeId/meal/:mealId/signoff", authenticator.validateToken, (req, res, next) => {
    logger.info("Post request at /api/studenthome/:homeId/meal");
    logger.debug("userId: " + req.userId);
    logger.debug("homeId: " + req.homeId);
    logger.debug("mealid: " + req.mealId);

    if (req.userId) {
        getParticipantCount(res, req.params.mealId, function (error, value) {
            if (value) {
                let whereClauseParticipant = "WHERE UserID = " + req.userId + " AND StudenthomeID = " + req.homeId + " AND MealID = " + req.mealId;
                queryHandler.getRecord("participants", whereClauseParticipant, function (error, rows) {
                    if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "User has not participating in this meal")) {
                        // Creates portions of the query with columns and values
                        let specifications = "UserID = " + req.userId + " AND StudenthomeID = " + req.homeId + " AND MealID = " + req.mealId;

                        // Posts the meal with the specifications
                        queryHandler.deleteRecordWithMoreIDs("participants", specifications, function (error, rows) {
                            if (valueHandler.checkRowsIsNotNull(res, error, rows, 401, "Could not remove participation")) {
                                res.status(200).json({
                                    userId: req.userId,
                                    studenthomeId: req.homeId,
                                    mealId: req.mealId
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});

router.get("/meal/:mealId/participants", authenticator.validateToken, (req, res, next) => {
    getParticipants(req, res, req.mealId, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "No participants found")) {
            res.status(200).json({
                rows
            });
        }
    });
});

router.get("/meal/:mealId/participants/:participantId", authenticator.validateToken, (req, res, next) => {
    getParticipants(req, res, req.mealId, function (error, rows) {
        let user = {};
        rows.forEach((participation) => {
            if (participation.UserID == req.params.participantId) {
                user = participation;
            }
        });
        if (user) {
            let whereClauseUser = "WHERE ID = " + req.params.participantId;
            queryHandler.getRecord("user", whereClauseUser, function (error, rows) {
                if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "No participants found")) {
                    res.status(200).json({
                        rows
                    });
                }
            });
        }
    });
});

function getParticipantCount(res, mealId, callback) {
    // Creates portions of the query with where clause
    let whereClauseMeal = "WHERE ID = " + mealId;
    let whereClauseParticipant = "WHERE MealID = " + mealId;

    // Requests the meal
    queryHandler.getRecord("meal", whereClauseMeal, function (error, rows) {
        if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "Meal does not exist")) {
            // Requests all participants who take part in the meal
            queryHandler.getRecord("participants", whereClauseParticipant, function (error, rowsParticipants) {
                // Checks wether the max number of participants isn't being exceeded
                logger.debug("Participants in meal: " + rowsParticipants.length);
                logger.debug("MaxParticipants: " + rows[0].MaxParticipants);
                if (rowsParticipants.length < rows[0].MaxParticipants) {
                    callback(error, true);
                } else {
                    res.status(401).json({ error: "Maximum number of participants exceeded" });
                    callback(error, false);
                }
            });
        }
    });
}

function getParticipants(req, res, id, callback) {
    if (req.userId) {
        let whereClause = "WHERE MealID = " + id;
        queryHandler.getRecord("participants", whereClause, function (error, rows) {
            if (valueHandler.checkRowsIsNotNull(res, error, rows, 404, "Meal does not have any participants")) {
                callback(error, rows);
            }
        });
    }
}

module.exports = router;
