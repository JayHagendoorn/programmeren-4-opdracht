const config = require("../config/config");
const logger = config.logger;

class Participants {
    constructor(userId, studenthomeId, mealId, signedUpOn) {
        try {
            this.userId = userId;
            this.studenthomeId = studenthomeId;
            this.mealId = mealId;
            this.signedUpOn = signedUpOn;
        } catch (err) {
            logger.error("Participant creation error: " + err);
            throw err;
        }
    }
}

module.exports = Participants;
