const config = require("../config/config");
const logger = config.logger;

class User {
    constructor(firstName, lastName, email, studentNumber, password) {
        try {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = this.validateEmail(email);
            this.studentNumber = studentNumber;
            this.password = password;
        } catch (err) {
            logger.error("User creation error: " + err);
            throw err;
        }
    }

    // Validates postal code to include @ and .
    validateEmail(email) {
        // Creates regex that checks characters for email
        let regex = new RegExp("^.+@[^.].*.[a-z]{2,}$");
        if (regex.test(email)) {
            logger.info("Validated email: " + email);
            return email;
        } else {
            logger.error("Invalid email");
            throw "Invalid email: " + email;
        }
    }
}

module.exports = User;
