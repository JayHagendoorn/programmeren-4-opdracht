const config = require("../config/config");
const logger = config.logger;

class Studenthome {
    constructor(name, address, housenumber, userId, postalcode, telephone, city) {
        try {
            this.name = this.validateValue(name);
            this.address = this.validateValue(address);
            this.housenumber = this.validateValue(housenumber);
            this.userId = this.validateValue(userId);
            this.postalcode = this.validatePostalCode(postalcode);
            this.telephone = this.validatePhoneNumber(telephone);
            this.city = this.validateValue(city);
        } catch (err) {
            logger.error("Studenthome creation error: " + err);
            throw err;
        }
    }

    // Validates a value on undefined
    validateValue(value){
        // Continues if attribute is not undefined
        if (value) {
            return value;
        } else {
            logger.error("Invalid credentials");
            throw ("Invalid credentials");
        } 
    }

    // Validates postal code to be 0000AA or 0000 AA
    validatePostalCode(postalCode) {
        // Creates regex that checks characters for postal code
        let regex = new RegExp("^[1-9][0-9]{3}[ ]?([A-RT-Z][A-Z]|[S][BCE-RT-Z])$");
        if (regex.test(postalCode)) {
            logger.info("Validated postal code: " + postalCode)
            return postalCode;
        } else {
            logger.error("Invalid postalcode");
            throw ("Invalid postalcode: " + postalCode);
        }
    }

    // Validates phone number to be 8 to 15 numbers
    validatePhoneNumber(telephone) {
        // Creates regex that checks characters telephone number
        let regex = new RegExp("^[0-9]{8,15}$");
        if (regex.test(telephone)) {
            logger.info("Validated telephone: " + telephone)
            return telephone;
        } else {
            logger.error("Invalid phone number: " + telephone);
            throw ("Invalid phone number: " + telephone);
        }
    }
}

module.exports = Studenthome;
