const config = require("../config/config");
const logger = config.logger;

class Meal {
    constructor(name, description, ingredients, allergies, creationDate, offerDate, price, userId, homeId, maxParticipants) {
        try {
            this.name = name;
            this.description = this.validateValue(description);
            this.ingredients = this.validateValue(ingredients);
            this.allergies = this.validateValue(allergies);
            this.creationDate = this.validateDate(creationDate);
            this.offerDate = this.validateDate(offerDate);
            this.price = this.validatePrice(price);
            this.userId = this.validateValue(userId);
            this.homeId = this.validateValue(homeId);
            this.maxParticipants = this.validateValue(maxParticipants);
        } catch (err) {
            logger.error("Meal creation error: " + err);
            throw err;
        }
    }

    // Validates a value on undefined
    validateValue(value) {
        // Continues if attribute is not undefined
        if (value) {
            return value;
        } else {
            logger.error("Invalid credentials");
            throw "Invalid credentials";
        }
    }

    // Validates price on number and negativity
    validatePrice(price) {
        let priceNum = parseFloat(price);
        if (priceNum >= 0) {
            let decimalprice = (Math.round(priceNum * 100) / 100).toFixed(2);
            logger.info("Validated price: " + decimalprice);
            return decimalprice;
        } else {
            logger.error("Invalid price");
            throw "Invalid price: " + price;
        }
    }

    // Validates date to be yyyy-mm-dd
    validateDate(date) {
        // Creates regex that checks characters for date
        let regex = new RegExp("^[0-9]{4}-[0-9]{2}-[0-9]{2}$");
        if (regex.test(date)) {
            logger.info("Validated date: " + date);
            return date;
        } else {
            logger.error("Invalid date format");
            throw "Invalid date format: " + date;
        }
    }
}

module.exports = Meal;
