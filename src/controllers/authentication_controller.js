const config = require("../config/config");
const logger = config.logger;
const assert = require("assert");
const jwt = require("jsonwebtoken");

module.exports = {
    // Checks if the given token is valid
    validateToken(req, res, next) {
        logger.info("validateToken called");
        logger.trace(req.headers);

        // Checks if there's something in the authentication header
        const authHeader = req.headers.authorization;
        if (!authHeader) {
            // Sends error when no authentication header was found
            logger.warn("Authorization header missing!");
             throw res.status(401).json({ error: "Missing Authorization" });
        }

        // Removes the first 7 characters from the received token (removes 'Bearer')
        const token = authHeader.substring(7, authHeader.length);

        // Check if the received token is correct
        jwt.verify(token, process.env.ENCRYPTIONKEY, (err, payload) => {
            if (err) {
                logger.warn("Not authorized");
                throw res.status(401).json({ error: "Unauthorized" });
            }
            // Continues if no error occurred and the payload was created
            if (payload) {
                logger.debug("token is valid", payload);
                req.userId = payload.id;
                next();
            }
        });
    }
};
