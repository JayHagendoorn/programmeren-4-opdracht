const express = require("express");
const bodyParser = require("body-parser");
const config = require("./src/config/config");
const logger = config.logger;
const mysql = require("mysql");
const pool = require("./src/config/db_handler")
require('dotenv').config()

// Requiring all routes
const pararoutes = require("./src/routes/parameter_routes");
const authroutes = require("./src/routes/authentication_routes");
const studenthomeroutes = require("./src/routes/studenthome_routes");
const mealroutes = require("./src/routes/meal_routes");
const participantroutes = require("./src/routes/participant_routes");

const app = express();

app.use(bodyParser.json());

const port = process.env.PORT || 3000;

// Before all request: Log method
app.all("*", (req, res, next) => {
    const method = req.method;
    logger.debug("Method: " + method);

    // Continues the call to the following route
    next();
});

// Sends info
app.get("/info", (req, res, next) => {
    const information = {
        creator: "Jay Hagendoorn",
        schoolyear: "Year 1",
        program: "Programmeren 4"
    };
    res.status(200).json(information);
});

// The routes
app.use("/api", pararoutes);
app.use("/api", authroutes);
app.use("/api", studenthomeroutes);
app.use("/api/studenthome", mealroutes);
app.use("/api", participantroutes);

// Sends error when endpoint doesn't exist
app.all("*", (req, res, next) => {
    const error = {
        error: "Endpoint does not exist"
    };
    res.status(404).json(error);
});

// Keeps listening to incoming requests
app.listen(port, () => {
    logger.info(`Samen Eten listening at http://localhost:${port}`);
});

module.exports = app;
